<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware([])->get('/cafes', function (Request $request) {
    $cafes = array (
        0 => 
        array (
          'id' => 1,
          'name' => 'Voomm',
          'address' => '8 Arkansas Place',
          'rating' => 3.1,
          'image' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjtn_J_csaGm-QxmfL1KG5okQ-uRSdIM2Xif5NjVK2Q8RUQqW38w&s',
          'latitude' => 55.1936526,
          'longitude' => 75.9684541,
        ),
        1 => 
        array (
          'id' => 2,
          'name' => 'Youbridge',
          'address' => '58 Basil Trail',
          'rating' => 4.4,
          'image' => 'https://d22ir9aoo7cbf6.cloudfront.net/wp-content/uploads/sites/2/2018/06/Working-Title-Burger-Bar-Multi-Concept-Stores-in-Singapore-Honeycombers.jpg',
          'latitude' => 39.704676,
          'longitude' => 122.955872,
        ),
        2 => 
        array (
          'id' => 3,
          'name' => 'Mita',
          'address' => '64456 Stone Corner Drive',
          'rating' => 2.9,
          'image' => 'https://d22ir9aoo7cbf6.cloudfront.net/wp-content/uploads/sites/2/2018/06/Working-Title-Burger-Bar-Multi-Concept-Stores-in-Singapore-Honeycombers.jpg',
          'latitude' => 4.9623026,
          'longitude' => 97.7525352,
        ),
        3 => 
        array (
          'id' => 4,
          'name' => 'Mudo',
          'address' => '18 Ohio Drive',
          'rating' => 3.2,
          'image' => 'https://i.pinimg.com/originals/d6/ca/9d/d6ca9d71ec76963066ab70ee6a75dde2.jpg',
          'latitude' => 45.521519,
          'longitude' => 3.5276642,
        ),
        4 => 
        array (
          'id' => 5,
          'name' => 'Trudeo',
          'address' => '0061 Trailsway Junction',
          'rating' => 3.6,
          'image' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjtn_J_csaGm-QxmfL1KG5okQ-uRSdIM2Xif5NjVK2Q8RUQqW38w&s',
          'latitude' => 22.843818,
          'longitude' => 114.164223,
        ),
        5 => 
        array (
          'id' => 6,
          'name' => 'Dabfeed',
          'address' => '203 Melby Point',
          'rating' => 4.1,
          'image' => 'https://d22ir9aoo7cbf6.cloudfront.net/wp-content/uploads/sites/2/2018/06/Working-Title-Burger-Bar-Multi-Concept-Stores-in-Singapore-Honeycombers.jpg',
          'latitude' => 59.3917649,
          'longitude' => 15.8447572,
        ),
        6 => 
        array (
          'id' => 7,
          'name' => 'Meeveo',
          'address' => '82520 Susan Alley',
          'rating' => 2.6,
          'image' => 'https://d22ir9aoo7cbf6.cloudfront.net/wp-content/uploads/sites/2/2018/06/Working-Title-Burger-Bar-Multi-Concept-Stores-in-Singapore-Honeycombers.jpg',
          'latitude' => -7.1697343,
          'longitude' => 108.8343538,
        ),
        7 => 
        array (
          'id' => 8,
          'name' => 'Topicshots',
          'address' => '51 Algoma Alley',
          'rating' => 2.2,
          'image' => 'https://d22ir9aoo7cbf6.cloudfront.net/wp-content/uploads/sites/2/2018/06/Working-Title-Burger-Bar-Multi-Concept-Stores-in-Singapore-Honeycombers.jpg',
          'latitude' => -14.116667,
          'longitude' => -73.616667,
        ),
        8 => 
        array (
          'id' => 9,
          'name' => 'Topiclounge',
          'address' => '15 Warner Plaza',
          'rating' => 4.0,
          'image' => 'https://d22ir9aoo7cbf6.cloudfront.net/wp-content/uploads/sites/2/2018/06/Working-Title-Burger-Bar-Multi-Concept-Stores-in-Singapore-Honeycombers.jpg',
          'latitude' => -12.1579355,
          'longitude' => 44.4146374,
        ),
        9 => 
        array (
          'id' => 10,
          'name' => 'Skyndu',
          'address' => '881 Washington Drive',
          'rating' => 4.2,
          'image' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjtn_J_csaGm-QxmfL1KG5okQ-uRSdIM2Xif5NjVK2Q8RUQqW38w&s',
          'latitude' => 41.529235,
          'longitude' => -8.5859581,
        ),
    );     
    return response()->json([
        'data' => $cafes,
    ]);
});