<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class cafeApiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/api/cafes');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' =>  [
                '*' => [
                    "id",
                    "name",
                    "address",
                    "rating",
                    "image",
                    "latitude",
                    "longitude"
                ]
            ]
        ]);
    }
}
